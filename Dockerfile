# version 3.9 works best with provided script
FROM python:3.9

# Set the working directory in the container
WORKDIR /app

# Clone the pollmaster repository and checkout the specific commit
# Since the normal variant of the Python image includes more tools, we don't need to install git separately
COPY ./pollmaster /app

# Install any needed packages specified in requirements.txt
RUN pip install --no-cache-dir -r /app/requirements.txt

# Define the command to run the pollmaster application
CMD ["python", "pollmaster.py"]
